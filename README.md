##  Snyk Integration Demonstration

This demonstration environment consists of an application that includes Snyk dependency scan. 

- The results are parsed from Snyk's JSON output to GitLab's dependency scanning json file output.

- Vulnerabilities are displayed in the Merge Request as well as the Pipeline Security tab.

- Vulnerabilities are included as part of the Vulnerability Report and listed as Dependency Scan - My Snyk Scanner.

- A policy is in place to create a Merge Request Approval for Critical, High, and Medium Severity vulnerabilities.


